var $ = jQuery.noConflict();
        
$(document).ready(function()
{
    var elem = $("#fluid_header_text");

    $(window).resize(function ()
    {
        
        if ($(window).width() >= 600 && $(window).width() <= 750 || $(window).width() >= 820) 
        {
            elem.removeClass('col-xs-12');
            if (elem.hasClass('col-xs-offset-4 col-xs-8 col-xs-offset-5 col-xs-7') == false)
            {				
                elem.addClass('col-xs-offset-4 col-xs-8 col-sm-offset-5 col-sm-7');
            }
        }
        else
        {
            elem.removeClass('col-xs-offset-4 col-xs-8 col-sm-offset-5 col-sm-7');
            if (elem.hasClass('col-xs-12') == false)
            {				
                elem.addClass('col-xs-12');
            }
        }
    });
	
    $(window).resize();
       
    
    $("#a_more").click(function(event)
    { 
      //отменяем стандартную обработку нажатия по ссылке
      event.preventDefault();

      //забираем идентификатор блока с атрибута href
      var id  = $(this).attr('href'),

      //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
      
      //анимируем переход на расстояние - top за 1500 мс
      $('body,html').animate({scrollTop: top}, 1000);
    });

        
    $("input[name='more_info']").click(function()
    {        
        var u_name = $(this).closest(".forma").find("[name='user_name']").val();
        var u_phone = $(this).closest(".forma").find("[name='user_phone']").val();
        
        if(u_name != "" && u_phone != "")
        {        
            sendMessage(1,u_name,u_phone,"Заявка успешно отправлена! Мы скоро Вам позвоним!");
        }
        else
        {
            alert("Введите имя и телефон...");
        }
    });
        
    $("input[name='send_phone']").click(function()
    {
        var u_name = $(this).closest(".forma").find("[name='user_name']").val();
        var u_phone = $(this).closest(".forma").find("[name='user_phone']").val();
        
        if(u_name != "" && u_phone != "")
        {
            sendMessage(2,u_name,u_phone,"Спасибо! Мы скоро Вам позвоним!");
        }
        else
        {
            alert("Введите имя и телефон...");
        }
    });
    
});


function sendMessage(mode, name, phone, message)
{             
    $.ajax({
        type:"POST",
        url:"/wp-content/themes/clean-template/mailing/sendMail.php",
        cache:false,
        data:{"username":name,"userphone":phone,"mode":mode},
        success:function(responce)
        {
            alert(message);
        }
    });
}