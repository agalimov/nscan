<?php
/*
Template Name: MainTemplateSite
*/
?>

<?php get_header(); ?> 

<div style="margin-left:0px;margin-right:0px;" class="row">
	<div class="col-xs-12">
		<div class="row top_image">
			<div style="position:relative" class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-offset-0 col-xs-12">
				<div class="col-xs-4 col-md-5 col-lg-4 cool_man"></div>
				<div class="col-xs-12" id="fluid_header_text">
					<p class="header_text">МЫ БУДЕМ С ВАМИ ОТ ДТП ДО СТРАХОВОЙ ВЫПЛАТЫ</p>
				</div>
				<div class="col-xs-offset-4 col-xs-8 col-sm-offset-5 col-sm-7">
					<p class="description_text">Быстрая и квалифицированная помощь участникам дорожно-транспортных происшествий. Прибывший на место ДТП аварийный комиссар полностью оформит происшествие (составление схемы ДТП, фотографирование повреждений, получение объяснений от участников), окажет психологическую помощь. </p>
				</div> 
				<a id="a_more" href="#more_info"><div class="col-sm-offset-5 col-sm-5 col-xs-offset-4 col-xs-8 buttons more">
					Узнать подробнее
				</div></a>
			</div>
		</div>
		
		<div id="more_info" class="advantages row">
			<div style="text-align:center" class="blocks col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
				<div class="block_header row">
					Преимущества работы с нами
				</div>
				<div class="row">
					<div class="col-xs-4">
						<img width=100% src="<?=get_template_directory_uri()?>/images/advantages/speed.png"/>
						<p class="mini_title"> Скорость </p>
						<p class="mini_description"> Время прибытия аварийного комиссара до 20 минут. Время оформления ДТП – от 10 минут. </p>
					</div>
					<div class="col-xs-4">
						<img width=100% src="<?=get_template_directory_uri()?>/images/advantages/accuracy.png"/>
						<p class="mini_title"> Точность </p>
						<p class="mini_description"> Комиссар сам составляет схему, фотографирует и оформляет документы. </p>
					</div>
					<div class="col-xs-4">
						<img width=100% src="<?=get_template_directory_uri()?>/images/advantages/economy.png"/>
						<p class="mini_title"> Экономия </p>
						<p class="mini_description"> Профессиональное оформление документов на месте ДТП сэкономит вам немало времени. </p>
					</div>
				</div>
			</div>
		</div>		
		
		<div class="services row">
			<div style="text-align:center" class="blocks col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
				<div class="block_header row">
					Наши услуги
				</div>
				
				<!-- Старая верстка услуг -->
				<!-- <div class="row">
					<div class="col-xs-12">
						<table style="width:100%">
							<tbody>
								<tr style="height:200px">
									<td class="content" style="width:28%; background-image: url(<?//=get_template_directory_uri()?>/images/services/departure.png)">
										<div>Выезд на место ДТП</div>
									</td>
									<td style="width:7%"></td>
									<td class="content" style="width:28%; background-image: url(<?//=get_template_directory_uri()?>/images/services/photo_video.png)">
										<div>Проведение фото/видео-фиксации, замеров</div>
									</td>
									<td style="width:7%"></td>
									<td class="content" style="width:28%; background-image: url(<?//=get_template_directory_uri()?>/images/services/judge.png)">
										<div>Юридические услуги по страховым спорам</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table style="width:100%; margin-top:-75px">
							<tbody>	
								<tr style="height:200px">
									<td style="width:18%"></td>
									<td class="content" style="width:28%; background-image: url(<?//=get_template_directory_uri()?>/images/services/documents.png)">
										<div>Составление и выдача  необходимых документов для ображения в страховую компанию</div>
									</td>
									<td style="width:7%"></td>
									<td class="content" style="width:28%; background-image: url(<?//=get_template_directory_uri()?>/images/services/help.png)">
										<div>Помощь в проведении независимой экспертизы и оценки ужерба авто</div>
									</td>
									<td style="width:18%"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div> -->
				
				<!-- Новая верстка услуг (таблица создана, т.к. div row не хочет принимать margin top -70) -->
				<table>
					<tbody>
						<tr>
							<td>
								<div class="row">
									<div class="col-xs-3 content">
										<img width=100% src="<?=get_template_directory_uri()?>/images/services/departure_txt.png"/>
										<!-- <div>Выезд на место ДТП</div> -->
									</div>
									<div class="col-xs-1 nocontent"></div>
									<div class="col-xs-3 content">
										<img width=100% src="<?=get_template_directory_uri()?>/images/services/photo_video_txt.png"/>
										<!-- <div>Проведение фото/видео-фиксации, замеров</div> -->
									</div>
									<div class="col-xs-1 nocontent"></div>
									<div class="col-xs-3 content">
										<img width=100% src="<?=get_template_directory_uri()?>/images/services/judge_txt.png"/>
										<!-- <div>Юридические услуги по страховым спорам</div> -->
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
				<table style="margin-top:-70px">
					<tbody>
						<tr>
							<td>
								<div class="row">
									<div style="width:16%" class="col-xs-2"></div>
									<div class="col-xs-3 content">
										<img width=100% src="<?=get_template_directory_uri()?>/images/services/documents_txt.png"/>
										<!-- <div>Составление и выдача  необходимых документов для ображения в страховую компанию</div> -->
									</div>
									<div class="col-xs-1 nocontent"></div>
									<div class="col-xs-3 content">
										<img width=100% src="<?=get_template_directory_uri()?>/images/services/help_txt.png"/>
										<!-- <div>Помощь в проведении независимой экспертизы и оценки ужерба авто</div> -->
									</div>
									<div class="col-xs-2"></div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="howwework row">
			<div style="text-align:center" class="blocks col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
				<div class="block_header row">
					Как мы работаем
				</div>
				<!-- Внутри этого row пустые комментарии не убирать! -->
				<div class="images row">
					<div class="image vcenter col-xs-2">
						<img width=100% src="<?=get_template_directory_uri()?>/images/howwork/auto.png"/>
					</div><!--
					--><div class="arrow vcenter col-xs-1">
						<img src="<?=get_template_directory_uri()?>/images/howwork/arrows.png"/>
					</div><!--
					--><div class="image vcenter col-xs-2">
						<img width=100% src="<?=get_template_directory_uri()?>/images/howwork/man_repair.png"/>
					</div><!--
					--><div class="arrow vcenter col-xs-1">
						<img src="<?=get_template_directory_uri()?>/images/howwork/arrows.png"/>
					</div><!--
					--><div class="image vcenter col-xs-2">
						<img width=100% src="<?=get_template_directory_uri()?>/images/howwork/money.png"/>
					</div><!--
					--><div class="arrow vcenter col-xs-1">
						<img src="<?=get_template_directory_uri()?>/images/howwork/arrows.png"/>
					</div><!--
					--><div class="image vcenter col-xs-2">
						<img width=100% src="<?=get_template_directory_uri()?>/images/howwork/man_jump.png"/>
					</div>
				</div>
				<div class="texts row">
					<div class="image vcenter col-xs-2">
						<p class="mini_description"> Произошло ДТП </p>
					</div>
					<div class="arrow vcenter col-xs-1">
					</div>
					<div class="image vcenter col-xs-2">
						<p class="mini_description"> Работа аварийного комиссара </p>
					</div>
					<div class="arrow vcenter col-xs-1">
					</div>
					<div class="image vcenter col-xs-2">
						<p class="mini_description"> Выплата страховки </p>
					</div>
					<div class="arrow vcenter col-xs-1">
					</div>
					<div class="image vcenter col-xs-2">
						<p style="margin-left:-20px" class="mini_description"> Клиент остается доволен </p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="testimonals row">
			<div class="blocks col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
				<div style="text-align:center" class="block_header row">
					Отзывы клиентов
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="image col-xs-3">
							<img width=100% src="<?=get_template_directory_uri()?>/images/person1.png"/>
						</div>
						<div class="col-xs-9">
							<div class="row">
								<div class="name col-xs-12">
									Владимир Иванов
								</div>
							</div>
							<div class="row">
								<div class="text col-xs-12">
									Пользовался их услугами компании “Приоритет” неоднократно.  Избавился от сомнительного удовольствия ожидания гибддшников в течение нескольких часов.
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="image col-xs-3">
							<img width=100% src="<?=get_template_directory_uri()?>/images/person2.png"/>
						</div>
						<div class="col-xs-9">
							<div class="row">
								<div class="name col-xs-12">
									Пётр Васильев
								</div>
							</div>
							<div class="row">
								<div class="text col-xs-12">
									Пользовался их услугами компании “Приоритет” неоднократно.  Избавился от сомнительного удовольствия ожидания гибддшников в течение нескольких часов.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contacts row">
			<div style="text-align:center" class="block_header row">
				<img src="<?=get_template_directory_uri()?>/images/slider.png"/>
				<br/>Контакты
			</div>
			<div class="row">
				<div id="map-location" class="col-xs-12"></div>
			</div>
			<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<script>
				ymaps.ready(init);
				function init()
				{
					var company = "<span style='font-weight:bold'>Служба аварийных комиссаров \"ПРИОРИТЕТ\"</span><br/>";
					var address = "г. Волжский, ул. Молодёжная, 12а";
					var myGeocoder = ymaps.geocode(address);
					myGeocoder.then(
						function (res) {							
							myMap = new ymaps.Map("map-location", {center: [res.geoObjects.get(0).geometry.getCoordinates()[0], res.geoObjects.get(0).geometry.getCoordinates()[1]], zoom: 13});
					
							myMap.controls.remove('searchControl');
					
							myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
								hintContent: company + address + " (вход с торца)",
								balloonContent: company + address + " (вход с торца)"
							}, {
								preset: 'islands#blueCircleDotIcon'
							});
							myMap.geoObjects.add(myPlacemark);							
						}
					);
				}
			</script>
		</div>
	</div>
</div>

<?php get_footer(); ?>