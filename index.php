<?php
/**
 * ������� �������� (index.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // ���������� header.php ?> 

<div class="row">
	<div class="col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
		<section>
			<?php if (have_posts()) : while (have_posts()) : the_post(); // ���� ����� ���� - ��������� ���� wp ?>
				<?php get_template_part('loop'); // ��� ����������� ������ ������ ����� ������ loop.php ?>
			<?php endwhile; // ����� �����
			else: echo '<h2>��� �������.</h2>'; endif; // ���� ������� ���, ������� "��������" ?>	 
			<?php pagination(); // ���������, ������� ���-�� � function.php ?>
		</section>
	</div>
</div>
<?php get_sidebar(); // ���������� sidebar.php ?>
<?php get_footer(); // ���������� footer.php ?>