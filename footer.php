<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
<footer>		
	<div class="row forms">
		<div style="text-align:center" class="col-xs-12">
			<div class="col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
				<div class="questions row">
					Хотите узнать подробнее о наших услугах?
				</div>
				<div class="forma row">
					<div class="col-sm-4 col-xs-6">
						<input type="text" class="forma-field" name="user_name" placeholder="Имя и фамилия"/>
					</div>
					<div class="col-sm-4 col-xs-6">
						<input type="text" class="forma-field" name="user_phone" placeholder="Телефон"/>
					</div>
					<div class="col-sm-4 col-xs-12">
						<input type="submit" class="forma-submit" name="more_info" value="Отправить заявку"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php include "company_info.php"; ?>
	
	<div class="row copyrt">
		© Все права защищены. Сайт разработан в <a href="http://lastweb.ru">Web For You</a>
	</div>
</footer>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
<script crossorigin="anonymous" async type="text/javascript" src="//api.pozvonim.com/widget/callback/v3/60699574fdf51822d8ac4493277fe364/connect" id="check-code-pozvonim" charset="UTF-8"></script>
</body>
</html>