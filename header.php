<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css/style.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/scripts.js"></script>
		
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	 
	<title><?php typical_title(); // выводи тайтл, функция лежит в function.php ?></title>
	<?php wp_head(); // необходимо для работы плагинов и функционала ?>
</head>
<body <?php body_class(); // все классы для body ?>>
	<header>
		<?php 
		
		include "company_info.php";
		
		/*$args = array( // опции для вывода верхнего меню, чтобы они работали, меню должно быть создано в админке
			'theme_location' => 'top', // идентификатор меню, определен в register_nav_menus() в function.php
			'container'=> 'nav', // обертка списка
			'menu_class' => 'bottom-menu', // класс для ul
			'menu_id' => 'bottom-nav', // id для ul
		);      
		wp_nav_menu($args); // выводим верхнее меню (см. ниже)
		*/
		?>
			
		<!-- Меню -->
		<div class="row menu">
			<div class="col-sm-offset-2 col-sm-8 col-xs-offset-0 col-xs-12">
			<?php foreach(wp_get_nav_menu_items('Menu') as $menu_item) { ?>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 item">
					<a href="<?=$menu_item->url?>"><?=$menu_item->title?></a>
				</div>
			<?php } ?> 
			</div>
		</div>
		
		<div id="modal" class="modal fade">
			<div id="modal_content" class="col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h2>Оставьте свой телефон и мы Вам обязательно позвоним!</h2>
				</div>
				<div class="modal-body forma">
					<div class="col-xs-6">
					  <input type="text" class="forma-field-call" name="user_name" placeholder="Имя и фамилия"/>
					</div>
					<div class="col-xs-6">
					  <input type="text" class="forma-field-call" name="user_phone" placeholder="Телефон"/>
					</div>
					<div class="col-xs-12">
					  <input type="submit" data-dismiss="modal" class="forma-submit-call" name="send_phone" value="Отправить"/>
					</div>
				</div>
			</div>
		</div>
		
	</header>